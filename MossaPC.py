#script che contiene la gestione della mossa del pc

import random, math

from typing import List, Tuple

import Costanti, GestisciMosse

def calcola_punteggio_mossa(schema:List[List[int]], mosse:List[Tuple[int]], giocatore:int, profondita:int ) -> float:
	#funzione che calcola il punteggio associato ad una mossa
	(nuovo_schema, punteggio_base) = GestisciMosse.esegui_turno(schema=schema, lista_mosse=mosse)
	#verifico la profondita
	if profondita <=0:
		return punteggio_base
	else:
		#vado a ricorsione
		#mi cambio giocatore
		if giocatore == Costanti.numero_giocatore_blu:
			giocatore_successivo = Costanti.numero_giocatore_rosso
		else:
			#ha giocato il rosso -> passo al blu
			giocatore_successivo = Costanti.numero_giocatore_blu
		#rovescio lo schema
		schema_rovesciato = GestisciMosse.ruota_schema(schema=nuovo_schema)
		lista_mosse_disponibili = GestisciMosse.calcola_lista_mosse_totale(schema=schema_rovesciato, numero_giocatore=giocatore_successivo)
		if len(lista_mosse_disponibili) == 0:
			#ha vinto
			return punteggio_base + Costanti.punteggio_vinci_partita
		else:
			#ho ancora mosse
			punteggio_avversario = max( [ calcola_punteggio_mossa(schema=schema_rovesciato, mosse=l, giocatore=giocatore_successivo, profondita=profondita-1) for l in lista_mosse_disponibili ] )
			#posso ritornare il punteggio trovato
			return punteggio_base - punteggio_avversario

def trova_mossa_migliore(schema:List[List[int]], giocatore:int, profondita:int ) -> List[Tuple[int, int]]:
	#metodo per ricavare la mossa migliore
	lista_mosse_migliori = []
	punteggio_migliore = -math.inf
	lista_mosse = GestisciMosse.calcola_lista_mosse_totale(schema=schema, numero_giocatore=giocatore)
	if len(lista_mosse) == 1:
		#non ha senso fare grandi conti, la mossa e obbligatoria
		return lista_mosse[0]
	for mossa in lista_mosse:
		punteggio = calcola_punteggio_mossa(schema=schema, mosse=mossa, giocatore=giocatore, profondita=profondita )
		if punteggio > punteggio_migliore:
			punteggio_migliore = punteggio
			lista_mosse_migliori = [mossa]
		elif punteggio == punteggio_migliore:
			lista_mosse_migliori.append(mossa)
	#al termine scelgo a caso tra la lista delle mosse migliori
	return random.choice(lista_mosse_migliori)