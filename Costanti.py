#modulo che contiene le costanti utilizzate
import os
#costanti varie
numero_giocatore_rosso = 1
numero_giocatore_blu = -1
numero_tipologia_giocatore_umano = 0
numero_tipologia_giocatore_computer = 1

numero_casella_libera = 0
numero_identificativo_pedina = 1
numero_identificativo_damone = 2
numero_casella_nera = 9

numero_pedina_giocatore_rosso = numero_identificativo_pedina
numero_damone_giocatore_rosso = numero_identificativo_damone
numero_pedina_giocatore_blu = -numero_identificativo_pedina
numero_damone_giocatore_blu = -numero_identificativo_damone


cartella_attuale = os.path.abspath(os.path.dirname(__file__))

#costanti per i punteggi
punteggio_mangiare_pedina = 1
punteggio_mangiare_damone = 2
punteggio_fai_damone = 0.5
punteggio_vinci_partita = 10
punteggio_pareggia = -3