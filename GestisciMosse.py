#script contenente il modulo per gestire le mosse

#presuppongo che gli schemi arrivino gia ruotati 

import PartitaDama, Costanti

from typing import List, Tuple


def copia_schema(schema:List[List[int]]) -> List[List[int]]:
	#metodo per copiare lo schema
	return [ riga[:] for riga in schema ]

def ruota_schema(schema:List[List[int]]) -> List[List[int]]:
		#metodo per ruotare la scacchiera di 180 gradi
		schema_attuale = [ r[::-1] for r in schema[::-1] ]
		return schema_attuale

##############################################################################################################################################################
#funzioni per eseguire le mosse

def esegui_mossa_singola(schema:List[List[int]], origine, destinazione)-> Tuple[List[List[int]], int]:
	#metodo per eseguire una mossa singola ed eventualmente mangiare la pedina
	schema_locale = copia_schema(schema=schema)
	(r_o, c_o) = origine
	(r_d, c_d) = destinazione
	numero_da_spostare = schema_locale[r_o][c_o]
	schema_locale[r_d][c_d] = numero_da_spostare
	schema_locale[r_o][c_o] = Costanti.numero_casella_libera
	punteggio_attuale = 0
	#verifico se devo mangiare
	if abs(r_d - r_o) >= 2:
		#devo mangiare
		pezzo_tolto = abs(schema_locale[(r_o+r_d)//2][(c_o+c_d)//2])
		#mi calcolo il relativo punteggio
		if pezzo_tolto == Costanti.numero_identificativo_pedina:
			punteggio_attuale = Costanti.punteggio_mangiare_pedina
		elif pezzo_tolto == Costanti.numero_identificativo_damone:
			punteggio_attuale = Costanti.punteggio_mangiare_damone
		else:
			#non dovrebbe mai capitare
			raise Exception('Numero pezzo mangiato non conforme')
		schema_locale[(r_o+r_d)//2][(c_o+c_d)//2] = Costanti.numero_casella_libera
	return (schema_locale, punteggio_attuale)

def esegui_sequenza_mosse(schema:List[List[int]], lista_mosse) -> Tuple[List[List[int]], int]:
	#funzione per eseguire una sequenza di mosse
	schema_locale = copia_schema(schema)
	punteggio_locale = 0
	#adesso eseguo tutte le mosse in successione
	for i in range(len(lista_mosse)-1):
		(schema_tmp, punteggio_tmp ) = esegui_mossa_singola(schema=schema_locale, origine=lista_mosse[i], destinazione=lista_mosse[i+1])
		schema_locale = schema_tmp
		punteggio_locale += punteggio_tmp
	#al termine ritorno la classica tupla
	return (schema_locale, punteggio_locale)

def verifica_nuovi_damoni(schema:List[List[int]], r_u, c_u) -> Tuple[List[List[int]], int]:
	#funzione per verificare se si deve aggiugnere un damone
	schema_locale = copia_schema(schema)
	punteggio=0
	if (r_u == 0) and (abs(schema_locale[r_u][c_u]) == Costanti.numero_identificativo_pedina ):
		#posso forse essere un damone
		punteggio = Costanti.punteggio_fai_damone
		#inserisco il damone
		numero_pedina = schema_locale[r_u][c_u]
		schema_locale[r_u][c_u] = int(Costanti.numero_identificativo_damone*numero_pedina/abs(numero_pedina))
	return (schema_locale, punteggio)

def esegui_turno(schema, lista_mosse) -> Tuple[List[List[int]], int]:
	#funzione per eseguire un intero turno con controllo anche dei damoni
	(nuovo_schema, punteggio) = esegui_sequenza_mosse(schema=schema, lista_mosse=lista_mosse)
	#verifico se con la mossa ho trovato un damone
	(r_u, c_u) = lista_mosse[-1]
	(schema_damone, punteggio_damone) = verifica_nuovi_damoni(schema=nuovo_schema, r_u=r_u, c_u=c_u)
	return (schema_damone, punteggio+punteggio_damone)
	

#############################################################################################################################################################################
#funzioni per calcolare l'elenco delle mosse, suppongo che sia obbligatorio mangiare, con priorita alle pedine per poi passare ai damoni e poi mosse normali
def calcola_lista_mosse_mangiare_pedina(schema, numero_giocatore) -> List[List[Tuple[int, int]]]:
	#funzione per calcolare l'elenco delle mosse che una pedina puo eseguire per mangiare 
	lista_mosse = []
	for r in range(len(schema)):
		for c in range(len(schema[r])):
			#verifico se e una pedina di quel giocatore
			if (abs(schema[r][c]) == Costanti.numero_identificativo_pedina) and ((schema[r][c]*numero_giocatore)>0):
				#e una pedina del giocatore in analisi
				#verifico se può mangiare
				lista_mosse_mangiare = calcola_lista_mosse_ricorsivo_mangiata_casella(schema=schema, r=r, c=c)
				lista_mosse.extend(lista_mosse_mangiare)
	return lista_mosse

def calcola_lista_mosse_mangiare_damone(schema, numero_giocatore) -> List[List[Tuple[int, int]]]:
	#funzione per calcolare l'elenco delle mosse che un damone puo eseguire per mangiare 
	lista_mosse = []
	for r in range(len(schema)):
		for c in range(len(schema[r])):
			#verifico se e una pedina di quel giocatore
			if (abs(schema[r][c]) == Costanti.numero_identificativo_damone) and ((schema[r][c]*numero_giocatore)>0):
				#e un damone del giocatore in analisi
				#verifico se può mangiare
				lista_mosse_mangiare = calcola_lista_mosse_ricorsivo_mangiata_casella(schema=schema, r=r, c=c)
				lista_mosse.extend(lista_mosse_mangiare)
	return lista_mosse

def calcola_lista_mosse_ricorsivo_mangiata_casella(schema, r, c) -> List[List[Tuple[int, int]]]:
	#metodo ricorsivo per popolare la lisat delle mosse possibili partendo da una determinata casella
	#mi calcolo le possibili direzioni
	lista_mosse = []
	lista_delta_r = [-2]
	lista_delta_c = [-2, 2]
	#verifico se e un damone
	if abs(schema[r][c]) == Costanti.numero_identificativo_damone:
		#aggiungo ancvhe le righe verso il passo
		lista_delta_r.append(2)
	dim_v = len(schema)
	dim_o = len(schema[0])
	for dr in lista_delta_r:
		for dc in lista_delta_c:
			#verifico che siano dentro i range
			if (0<=(r+dr)<dim_v) and (0<= (c+dc) < dim_o ):
				#devo verificare che la casella di destinazione sia vuota, che la casella davanti sia piena, con un pezzo di squadra diversa e contenente un pezzo che si possa mangiare
				if (schema[r+dr][c+dc] == Costanti.numero_casella_libera) and (schema[r+dr//2][c+dc//2] != Costanti.numero_casella_libera) and ((schema[r+dr//2][c+dc//2]*schema[r][c])<0) and ( abs(schema[r+dr//2][c+dc//2])<=abs(schema[r][c]) ):
					#posso mangiare
					origine = (r,c)
					dest = (r+dr, c+dc)
					lista_caselle_mossa = [ origine, dest ]
					(schema_successivo, punteggio) = esegui_mossa_singola(schema=schema, origine=origine, destinazione=dest)
					lista_mosse_successive = calcola_lista_mosse_ricorsivo_mangiata_casella(schema=schema_successivo, r=r+dr, c=c+dc)
					if len(lista_mosse_successive) >0:
						#le devo aggiungere a quelle trovate
						for mosse_succ in lista_mosse_successive:
							lista_mosse.append( lista_caselle_mossa + mosse_succ[1:] ) #tolgo il primo perche altrimenti sarebbe uguale all'ultimo di questa mossa
					else:
						#aggiungo la mossa semplice
						lista_mosse.append(lista_caselle_mossa)
	return lista_mosse

def calcola_lista_mosse_movimento(schema, numero_giocatore) -> List[List[Tuple[int, int]]]:
	#funzione per calcolare la lisat delle mosse di un movimento semplice
	lista_mosse = []
	dim_v = len(schema)
	dim_o = len(schema[0])
	for r in range(len(schema)):
		for c in range(len(schema[r])):
			#verifico se e una pedina di quel giocatore
			if (schema[r][c]*numero_giocatore)>0:
				#calcolo le possibili direzioni
				lista_delta_r = [-1]
				lista_delta_c = [-1, 1]
				#verifico se e un damone
				if abs(schema[r][c]) == Costanti.numero_identificativo_damone:
					#aggiungo ancvhe le righe verso il passo
					lista_delta_r.append(1)
				for dr in lista_delta_r:
					for dc in lista_delta_c:
						#verifico che siano dentro i range
						if (0<=(r+dr)<dim_v) and (0<= (c+dc) < dim_o ):
							#devo verificare che la casella di destinazione sia vuota
							if (schema[r+dr][c+dc] == Costanti.numero_casella_libera) :
								lista_mosse.append( [(r,c), (r+dr, c+dc)] )
	return lista_mosse

def calcola_lista_mosse_totale(schema, numero_giocatore) -> List[List[Tuple[int, int]]]:
	#funzione per calcolare la lisat completa delle mosse seguendo la logica descritta sopra
	lista_mangiare_pedina = calcola_lista_mosse_mangiare_pedina(schema=schema, numero_giocatore=numero_giocatore)
	if len(lista_mangiare_pedina)>0:
		return lista_mangiare_pedina
	#altrimenti provo con i damoni
	lista_mangiare_damoni = calcola_lista_mosse_mangiare_damone(schema=schema, numero_giocatore=numero_giocatore)
	if len(lista_mangiare_damoni)>0:
		return lista_mangiare_damoni
	#altrimenti calcolo le mosse di movimento
	lista_movimento = calcola_lista_mosse_movimento(schema=schema, numero_giocatore=numero_giocatore)
	return lista_movimento











