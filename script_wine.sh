#/bin/bash

python3 -c 'import pyinstaller_versionfile;

pyinstaller_versionfile.create_versionfile(
    output_file="versionfile.txt",
    version="1.1.0.0",
    company_name="Riccardo Bortoluzzi",
    file_description="Gioco della dama semplice con varie difficoltà",
    internal_name="dama_profondita",
    legal_copyright="",
    original_filename="dama_profondita.exe",
    product_name="Dama semplice"
)
'

#cambiare / in linux con ^ in windows
#; per Widnows, : per linux per add-binary

#				--specpath ./spec_file \

wine python -m pip install --upgrade pip
wine python -m pip install --upgrade -r requirements.txt
wine pyinstaller --distpath ./dist_compilata \
				--clean \
				--onefile \
				--name dama_profondita \
				--add-binary "./immagini_pedine/*;immagini_pedine" \
				--hiddenimport typing \
				--hiddenimport PyQt5 \
				--hiddenimport sys \
				--hiddenimport os \
				--hiddenimport time \
				--hiddenimport random \
				--hiddenimport math \
				--noconsole \
				--icon ./spec_file/icona_dama.ico \
				--version-file versionfile.txt \
				main.py

