#groupbox che si chiama groupBox_schema

def crea_schema_gioco(self):
	#metodo per creare lo shcema di gioco
	#recupero la dimensione
	dimensione_scacchiera = int(self.grafica.spinBox_dimensione.value())
	riga_pari = [Costanti.numero_casella_libera, Costanti.numero_casella_nera]*dimensione_scacchiera
	riga_dispari = [Costanti.numero_casella_nera, Costanti.numero_casella_libera]*dimensione_scacchiera
	self.schema_attuale = []
	self.dizionario_pulsanti = {}
	for i in range(dimensione_scacchiera):
		if (i%2) == 0:
			self.schema_attuale.append(riga_pari[:dimensione_scacchiera])
		else:
			#riga dispari
			self.schema_attuale.append(riga_dispari[:dimensione_scacchiera])
	#adesso creo effettivamnete i bottoni 
	#for i in self.grafica.gridLayout_3.children():
	for i in self.grafica.groupBox_schema.children():
		if not(isinstance(i, QtWidgets.QLayout)):
			i.deleteLater()
	#self.grafica.gridLayout_3 = QtWidgets.QGridLayout(self.grafica.groupBox_schema)
	for r in range(dimensione_scacchiera):
		for c in range(dimensione_scacchiera):
			if self.schema_attuale[r][c] == Costanti.numero_casella_nera:
				#metto una gl semplice
				openGL = QtWidgets.QOpenGLWidget(self.grafica.groupBox_schema)
				self.grafica.gridLayout_3.addWidget(openGL, r, c, 1, 1)
			elif self.schema_attuale[r][c] == Costanti.numero_casella_libera:
				#metto il pulsante
				pulsante = QtWidgets.QPushButton(self.grafica.groupBox_schema)
				pulsante.setIconSize(QtCore.QSize(self.dimensione_icona, self.dimensione_icona))
				pulsante.setMinimumSize(self.dimensione_casella, self.dimensione_casella)
				pulsante.setMaximumSize(self.dimensione_casella, self.dimensione_casella)
				#pulsante.setCheckable(True)
				self.grafica.gridLayout_3.addWidget(pulsante, r, c, 1, 1)
				#str_tupla_riga_colonne = str((r, c))
				#pulsante.setText(f'{r}{c}')
				self.dizionario_pulsanti[pulsante] = (r, c)
				#metto la funzione
				pulsante.pressed.connect(self.pressione_casella)


def inserisci_immagini_pedine(self):
	#metodo per inserire le pedine come immagini
	for obj in self.grafica.groupBox_schema.children():
		if isinstance(obj, QtWidgets.QPushButton):
			if obj in self.dizionario_pulsanti.keys():
				(r, c) = self.dizionario_pulsanti[obj]
				#verifico cosa devo inserire
				if self.schema_attuale[r][c] == Costanti.numero_casella_libera:
					#rimuovo eventualmente l'icona
					path_immagine = self.immagine_icona_vuota
				elif self.schema_attuale[r][c] == Costanti.numero_pedina_giocatore_blu:
					#devo aggiugnere l'icona
					path_immagine =self.immagine_pedina_blu
				elif self.schema_attuale[r][c] == Costanti.numero_damone_giocatore_blu:
					#devo aggiugnere l'icona
					path_immagine = self.immagine_damone_blu
				elif self.schema_attuale[r][c] == Costanti.numero_pedina_giocatore_rosso:
					#devo aggiugnere l'icona
					path_immagine = self.immagine_pedina_rosso
				elif self.schema_attuale[r][c] == Costanti.numero_damone_giocatore_rosso:
					#devo aggiugnere l'icona
					path_immagine = self.immagine_damone_rosso
				#inserisco l'icona
				icona = QtGui.QIcon()
				icona.addPixmap(QtGui.QPixmap(path_immagine), QtGui.QIcon.Normal, QtGui.QIcon.Off,)
				obj.setIcon(icona)

def seleziona_caselle(self):
	#metodo per selezionare solo gli elementi presenti nella lista
	for obj in self.grafica.groupBox_schema.children():
		if isinstance(obj, QtWidgets.QPushButton):
			#if obj in self.dizionario_pulsanti.keys():
			#adesso mi calcolo i possibili path delle immagini
			(r, c) = self.dizionario_pulsanti[obj]
			#verifico cosa devo inserire
			if self.schema_attuale[r][c] == Costanti.numero_casella_libera:
				#rimuovo eventualmente l'icona
				icona_selezionata = self.immagine_icona_vuota_sel
				icona_normale = self.immagine_icona_vuota
			elif self.schema_attuale[r][c] == Costanti.numero_pedina_giocatore_blu:
				#devo aggiugnere l'icona
				icona_selezionata =self.immagine_pedina_blu_sel
				icona_normale =self.immagine_pedina_blu
			elif self.schema_attuale[r][c] == Costanti.numero_damone_giocatore_blu:
				#devo aggiugnere l'icona
				icona_selezionata = self.immagine_damone_blu_sel
				icona_normale =self.immagine_damone_blu
			elif self.schema_attuale[r][c] == Costanti.numero_pedina_giocatore_rosso:
				#devo aggiugnere l'icona
				icona_selezionata = self.immagine_pedina_rosso_sel
				icona_normale =self.immagine_pedina_rosso
			elif self.schema_attuale[r][c] == Costanti.numero_damone_giocatore_rosso:
				#devo aggiugnere l'icona
				icona_selezionata = self.immagine_damone_rosso_sel
				icona_normale =self.immagine_damone_rosso
			#verifico se devo selezionarlo o meno
			if self.dizionario_pulsanti[obj] in self.lista_selezionati:
				path_immagine = icona_selezionata
			else:
				path_immagine = icona_normale
			#verifico se il pulsante deve essere attivo o meno
			#non funziona perche disattivati si vedono
			if (self.schema_attuale[r][c] != Costanti.numero_casella_libera) and ((self.schema_attuale[r][c] * self.giocatore_attuale) >0):
				#devo verificare se e pc o umano
				if ( (self.giocatore_attuale == Costanti.numero_giocatore_blu) and (self.tipologia_giocatore_blu == Costanti.numero_tipologia_giocatore_umano) ) or ( (self.giocatore_attuale == Costanti.numero_giocatore_rosso) and (self.tipologia_giocatore_rosso == Costanti.numero_tipologia_giocatore_umano) ):
					#devo abilitarlo
					obj.setEnabled(True)
				else:
					#lo disabilito
					obj.setDisabled(True)
			else:
				#bottone attivo per spostamenti
				obj.setEnabled(True)
			#inserisco l'icona
			icona = QtGui.QIcon()
			icona.addPixmap(QtGui.QPixmap(path_immagine), QtGui.QIcon.Normal, QtGui.QIcon.Off)
			obj.setIcon(icona)
			icona_disattivata = QtGui.QIcon()
			icona_disattivata.addPixmap(QtGui.QPixmap(path_immagine), QtGui.QIcon.Disabled, QtGui.QIcon.Off)
			obj.setIcon(icona_disattivata)


def pressione_casella(self):
	#metodo per gestire la pressione di un pulsante
	#mi ricavo il pulsante premuto
	flag_trovato = False
	for obj in self.grafica.groupBox_schema.children():
		if isinstance(obj, QtWidgets.QPushButton):
			if obj.isDown():
				tupla_trovata = self.dizionario_pulsanti[obj]
				flag_trovato = True
				#print(tupla_trovata)
				break
	if flag_trovato:
		#ho trovato riga e colonna
		(riga, colonna) = tupla_trovata
		#verifico se ho scelto di deselezionare una case3lla
		if (len(self.lista_selezionati) >0 ) and (tupla_trovata == self.lista_selezionati[-1]):
			#posso rimuoverla
			self.lista_selezionati.remove(tupla_trovata)
			#a questo punto riaggiorno tutto
			self.aggiorna_schema_grafica()
		else:
			#verifico se ho selezionato un path accettabile
			numero_elementi = len(self.lista_selezionati) +1
			if (self.lista_selezionati + [tupla_trovata]) in [ mossa[:numero_elementi] for mossa in self.lista_mosse_disponibili ]:
				#significa che ho selezionato un path accettabile
				#se sono a meta di una mossa la eseguo
				if len(self.lista_selezionati)>0:
					ultima_casella = self.lista_selezionati[-1]
					(nuovo_schema, punteggio) = GestisciMosse.esegui_mossa_singola(schema=self.schema_attuale, origine=ultima_casella, destinazione=tupla_trovata)
					self.schema_attuale = nuovo_schema
				#aggiungo la tupla a quelle selezionate
				self.lista_selezionati.append(tupla_trovata)
				#adesso devo verificare se ho completato una mossa
				if self.lista_selezionati in self.lista_mosse_disponibili:
					#mossa completata
					nuovo_thread = Thread(target=self.termina_turno_utente)
					nuovo_thread.start()
					#self.termina_turno_utente()
				else:
					#a questo punto riaggiorno tutto
					self.aggiorna_schema_grafica()


def esegui_mossa_computer(self):
	#metodo per gestire la mossa del computer, lo schema dovrebbe essere gia girato e il giocatore quello del pc
	if len(self.lista_mosse_disponibili)>0:
		#ho qualche mossa, altrimenti mi fermo e non faccio niente
		mossa_migliore_scelta = MossaPC.trova_mossa_migliore(schema=self.schema_attuale, giocatore=self.giocatore_attuale, profondita=self.profondita)
		#adesso devo mostrare la mossa al giocatore
		#mi calcolo l'elenco con le coordinate in modod che l'utenta veda
		dim_v = len(self.schema_attuale)
		dim_o = len(self.schema_attuale[0])
		coordinate_visibili_utente = [ (dim_v-r-1, dim_o-c-1) for (r,c) in mossa_migliore_scelta ]
		print(coordinate_visibili_utente)
		#rigiro lo schema
		schema_ruotato = GestisciMosse.ruota_schema(schema=self.schema_attuale)
		self.schema_attuale = schema_ruotato
		#lo mostro inizialmente
		self.aggiorna_schema_grafica()
		#adesso eseguo una ad una le mosse del pc
		for casella in coordinate_visibili_utente: #escludo l'ultima che la prendo a parte, provo a prenderli tutti
			#se ne ho gia inserita una tra i selezionati allora eseguo la mossa
			if len(self.lista_selezionati)>0:
				(nuovo_schema, punteggio) = GestisciMosse.esegui_mossa_singola(schema=self.schema_attuale, origine=self.lista_selezionati[-1], destinazione=casella)
				self.schema_attuale = nuovo_schema
			#aggiungo la casella a quelli selezionati
			self.lista_selezionati.append(casella)
			#aggiorno la grafica
			self.aggiorna_schema_grafica()
			#attendo un tempo prestabilito
			time.sleep(self.tempo_attesa_mossa_pc)
		#al termine eseguo l'ultima mossa
		#(nuovo_schema, punteggio) = GestisciMosse.esegui_mossa_singola(schema=self.schema_attuale, origine=self.lista_selezionati[-1], destinazione=coordinate_visibili_utente[-1])
		#self.schema_attuale = nuovo_schema
		#adesso devo rigirare lo schema 2 volte per verificare se ci sono nuovi damoni
		schema_ruotato = GestisciMosse.ruota_schema(schema=self.schema_attuale)
		(r, c) = mossa_migliore_scelta[-1]
		(nuovo_schema, punteggio) = GestisciMosse.verifica_nuovi_damoni(schema=schema_ruotato, r_u=r, c_u=c)
		self.schema_attuale = GestisciMosse.ruota_schema(schema=nuovo_schema)
		#adesso cambio giocatore
		#inverto il giocatore attuale
		if self.giocatore_attuale == Costanti.numero_giocatore_blu:
			self.giocatore_attuale = Costanti.numero_giocatore_rosso
		else:
			#ha giocato il rosso -> passo al blu
			self.giocatore_attuale = Costanti.numero_giocatore_blu
		#mostro lo schema cosi come e
		self.aggiorna_schema_grafica()
		#inizializzo il turno
		self.inizializza_turno()