#script che contiene la classe per giocare la partita a dama
from typing import Dict, List, Tuple
from GraficaDama import Ui_MainWindow_dama
from PyQt5 import QtCore, QtGui, QtWidgets

import Costanti, GestisciMosse, MossaPC

import sys, os, time

class Worker(QtCore.QObject):
	finished = QtCore.pyqtSignal()
	aggiornamento = QtCore.pyqtSignal()

	def esegui_mossa_computer(self, oggetto_partita):
		if len(oggetto_partita.lista_mosse_disponibili)>0:
			#ho qualche mossa, altrimenti mi fermo e non faccio niente
			mossa_migliore_scelta = MossaPC.trova_mossa_migliore(schema=oggetto_partita.schema_attuale, giocatore=oggetto_partita.giocatore_attuale, profondita=oggetto_partita.profondita)
			#adesso devo mostrare la mossa al giocatore
			#mi calcolo l'elenco con le coordinate in modod che l'utenta veda
			dim_v = len(oggetto_partita.schema_attuale)
			dim_o = len(oggetto_partita.schema_attuale[0])
			coordinate_visibili_utente = [ (dim_v-r-1, dim_o-c-1) for (r,c) in mossa_migliore_scelta ]
			print(coordinate_visibili_utente)
			#rigiro lo schema
			schema_ruotato = GestisciMosse.ruota_schema(schema=oggetto_partita.schema_attuale)
			oggetto_partita.schema_attuale = schema_ruotato
			#lo mostro inizialmente
			#self.aggiorna_schema_grafica()
			self.aggiornamento.emit()
			#adesso eseguo una ad una le mosse del pc
			for casella in coordinate_visibili_utente: #escludo l'ultima che la prendo a parte, provo a prenderli tutti
				#se ne ho gia inserita una tra i selezionati allora eseguo la mossa
				if len(oggetto_partita.lista_selezionati)>0:
					(nuovo_schema, punteggio) = GestisciMosse.esegui_mossa_singola(schema=oggetto_partita.schema_attuale, origine=oggetto_partita.lista_selezionati[-1], destinazione=casella)
					oggetto_partita.schema_attuale = nuovo_schema
				#aggiungo la casella a quelli selezionati
				oggetto_partita.lista_selezionati.append(casella)
				#aggiorno la grafica
				#self.aggiorna_schema_grafica()
				self.aggiornamento.emit()
				#attendo un tempo prestabilito
				time.sleep(oggetto_partita.tempo_attesa_mossa_pc)
			#al termine eseguo l'ultima mossa
			#(nuovo_schema, punteggio) = GestisciMosse.esegui_mossa_singola(schema=oggetto_partita.schema_attuale, origine=oggetto_partita.lista_selezionati[-1], destinazione=coordinate_visibili_utente[-1])
			#oggetto_partita.schema_attuale = nuovo_schema
			#adesso devo rigirare lo schema 2 volte per verificare se ci sono nuovi damoni
			schema_ruotato = GestisciMosse.ruota_schema(schema=oggetto_partita.schema_attuale)
			(r, c) = mossa_migliore_scelta[-1]
			(nuovo_schema, punteggio) = GestisciMosse.verifica_nuovi_damoni(schema=schema_ruotato, r_u=r, c_u=c)
			oggetto_partita.schema_attuale = GestisciMosse.ruota_schema(schema=nuovo_schema)
			#adesso cambio giocatore
			#inverto il giocatore attuale
			if oggetto_partita.giocatore_attuale == Costanti.numero_giocatore_blu:
				oggetto_partita.giocatore_attuale = Costanti.numero_giocatore_rosso
			else:
				#ha giocato il rosso -> passo al blu
				oggetto_partita.giocatore_attuale = Costanti.numero_giocatore_blu
			#mostro lo schema cosi come e
			#self.aggiorna_schema_grafica()
			self.aggiornamento.emit()
			#inizializzo il turno
			oggetto_partita.inizializza_turno()
		#al termine emetto il segnale di file
		self.finished.emit()

class PartitaDama(QtWidgets.QMainWindow):
	#modulo che apre il padre e dopo utilizza la grafica del rispettivo modulo

	
	cartella_immagini = os.path.join(Costanti.cartella_attuale, 'immagini_pedine')

	grafica : Ui_MainWindow_dama
	giocatore_attuale : int
	schema_attuale : List[List[int]]
	tipologia_giocatore_rosso : int
	tipologia_giocatore_blu : int
	profondita : int
	profondita_iniziale :int
	pedine_iniziali : int
	dizionario_pulsanti:Dict[QtWidgets.QPushButton, Tuple[int, int]]
	lista_selezionati:List[Tuple[int, int]]
	lista_mosse_disponibili : List[List[Tuple[int, int]]]

	immagine_pedina_rosso = os.path.join(cartella_immagini, 'pedina_rossa.png')
	immagine_damone_rosso = os.path.join(cartella_immagini, 'damone_rosso.png')
	immagine_pedina_blu = os.path.join(cartella_immagini, 'pedina_blu.png')
	immagine_damone_blu = os.path.join(cartella_immagini, 'damone_blu.png')
	immagine_icona_vuota = os.path.join(cartella_immagini, 'icona_vuota.png')
	immagine_casella_nera = os.path.join(cartella_immagini, 'casella_nera.png')

	immagine_pedina_rosso_sel = os.path.join(cartella_immagini, 'pedina_rossa_sel.png')
	immagine_damone_rosso_sel = os.path.join(cartella_immagini, 'damone_rosso_sel.png')
	immagine_pedina_blu_sel = os.path.join(cartella_immagini, 'pedina_blu_sel.png')
	immagine_damone_blu_sel = os.path.join(cartella_immagini, 'damone_blu_sel.png')
	immagine_icona_vuota_sel = os.path.join(cartella_immagini, 'icona_vuota_sel.png')


	dimensione_icona = 50
	dimensione_casella = 50

	tempo_attesa_mossa_pc =0





	def __init__(self) -> None:
		app = QtWidgets.QApplication(sys.argv)
		super().__init__()
		self.grafica = Ui_MainWindow_dama()
		self.grafica.setupUi(self)
		#qui le operazioni iniziali eventualmente
		#self.inserisci_righe_a_caso()
		self.show()
		sys.exit(app.exec_())

	############################################################################################################################################################
	#metodi per creare una nuova partita

	def crea_nuova_partita(self):
		#metodo per cominicare una nuova partita
		#mi ricavo i parametri
		self.giocatore_attuale = Costanti.numero_giocatore_blu
		self.profondita = int(self.grafica.spinBox_profondita.value())
		self.profondita_iniziale = int(self.grafica.spinBox_profondita.value())
		#verifico come ho scelto di giocare
		if self.grafica.radioButton_1_giocatore.isChecked():
			#1 contro pc
			self.tipologia_giocatore_blu = Costanti.numero_tipologia_giocatore_umano
			self.tipologia_giocatore_rosso = Costanti.numero_tipologia_giocatore_computer
		elif self.grafica.radioButton_2_giocatori.isChecked():
			#1 contro 1
			self.tipologia_giocatore_blu = Costanti.numero_tipologia_giocatore_umano
			self.tipologia_giocatore_rosso = Costanti.numero_tipologia_giocatore_umano
		#creo lo schema
		self.crea_schema_gioco()
		self.inserisci_righe_pedine()
		self.inizializza_turno()
		self.aggiorna_schema_grafica()
		

	def inserisci_righe_pedine(self):
		#metodo per modificare lo schema con i numeri iniziali delle pedine
		#mi ricavo il numero di righe
		numero_righe_pedine = int(self.grafica.spinBox_righe_pedine.value())
		self.pedine_iniziali = 0 #incremento anche questo numero
		for r in range(numero_righe_pedine):
			for c in range(len(self.schema_attuale[0])):
				#analizzo tutte le prime n righe e ultime n righe
				#alto
				if self.schema_attuale[r][c] == Costanti.numero_casella_libera:
					#ci metto il giocatore rosso
					self.schema_attuale[r][c] = Costanti.numero_pedina_giocatore_rosso
					#incremento il numero di pedine
					self.pedine_iniziali += 1
				#basso
				if self.schema_attuale[-r-1][c] == Costanti.numero_casella_libera:
					#giocatore blu
					self.schema_attuale[-r-1][c] = Costanti.numero_pedina_giocatore_blu

	def crea_schema_gioco(self):
		#metodo per creare lo shcema di gioco
		#recupero la dimensione
		dimensione_scacchiera = int(self.grafica.spinBox_dimensione.value())
		riga_pari = [Costanti.numero_casella_libera, Costanti.numero_casella_nera]*dimensione_scacchiera
		riga_dispari = [Costanti.numero_casella_nera, Costanti.numero_casella_libera]*dimensione_scacchiera
		self.schema_attuale = []
		self.dizionario_pulsanti = {}
		for i in range(dimensione_scacchiera):
			if (i%2) == 0:
				self.schema_attuale.append(riga_pari[:dimensione_scacchiera])
			else:
				#riga dispari
				self.schema_attuale.append(riga_dispari[:dimensione_scacchiera])
		#elimino la tabella attuale
		self.grafica.tableWidget_schema.clear()
		self.grafica.tableWidget_schema.setRowCount(0)
		self.grafica.tableWidget_schema.setColumnCount(0)
		self.grafica.tableWidget_schema.setRowCount(dimensione_scacchiera)
		self.grafica.tableWidget_schema.setColumnCount(dimensione_scacchiera)
		for r in range(dimensione_scacchiera):
			for c in range(dimensione_scacchiera):
				#mi vado a popolare le dimensioni
				self.grafica.tableWidget_schema.horizontalHeader().setDefaultSectionSize(self.dimensione_casella)
				self.grafica.tableWidget_schema.horizontalHeader().setMinimumSectionSize(self.dimensione_casella)
				self.grafica.tableWidget_schema.verticalHeader().setDefaultSectionSize(self.dimensione_casella)
				self.grafica.tableWidget_schema.verticalHeader().setMinimumSectionSize(self.dimensione_casella)
				if self.schema_attuale[r][c] == Costanti.numero_casella_nera:
					#metto l'immagine nera
					pic =QtGui.QPixmap(self.immagine_casella_nera)
					immagine_scalata = pic.scaledToHeight(self.dimensione_icona)
					label = QtWidgets.QLabel()
					label.setPixmap(immagine_scalata)
					self.grafica.tableWidget_schema.setCellWidget(r, c, label)

	def seleziona_caselle(self):
		#metodo per selezionare solo gli elementi presenti nella lista
		for r in range(len(self.schema_attuale)):
			for c in range(len(self.schema_attuale[0])):
				tupla_coordinate = (r,c)
				#verifico di non avere una casella nera
				if self.schema_attuale[r][c] != Costanti.numero_casella_nera:
					#verifico cosa devo inserire
					if self.schema_attuale[r][c] == Costanti.numero_casella_libera:
						#rimuovo eventualmente l'icona
						icona_selezionata = self.immagine_icona_vuota_sel
						icona_normale = self.immagine_icona_vuota
					elif self.schema_attuale[r][c] == Costanti.numero_pedina_giocatore_blu:
						#devo aggiugnere l'icona
						icona_selezionata =self.immagine_pedina_blu_sel
						icona_normale =self.immagine_pedina_blu
					elif self.schema_attuale[r][c] == Costanti.numero_damone_giocatore_blu:
						#devo aggiugnere l'icona
						icona_selezionata = self.immagine_damone_blu_sel
						icona_normale =self.immagine_damone_blu
					elif self.schema_attuale[r][c] == Costanti.numero_pedina_giocatore_rosso:
						#devo aggiugnere l'icona
						icona_selezionata = self.immagine_pedina_rosso_sel
						icona_normale =self.immagine_pedina_rosso
					elif self.schema_attuale[r][c] == Costanti.numero_damone_giocatore_rosso:
						#devo aggiugnere l'icona
						icona_selezionata = self.immagine_damone_rosso_sel
						icona_normale =self.immagine_damone_rosso
					#verifico se devo selezionarlo o meno
					if tupla_coordinate in self.lista_selezionati:
						path_immagine = icona_selezionata
					else:
						path_immagine = icona_normale
					#verifico se il pulsante deve essere attivo o meno
					pic =QtGui.QPixmap(path_immagine)
					immagine_scalata = pic.scaledToHeight(self.dimensione_icona)
					label = QtWidgets.QLabel()
					label.setPixmap(immagine_scalata)
					self.grafica.tableWidget_schema.setCellWidget(r, c, label)

	def aggiorna_schema_grafica(self):
		#metodo per aggiornare lo schema della grafica
		#self.inserisci_immagini_pedine()
		self.seleziona_caselle()
		#modifico il colore del giocatore
		if self.giocatore_attuale == Costanti.numero_giocatore_blu:
			colore = 'BLU'
		else:
			#qua sicuramente c'e il rosso
			colore='ROSSO'
		self.grafica.label_giocatore.setText(colore)

	def inizializza_turno(self):
		#metodo per inizializzare il turno liberando la lista dei selezionati e popolando la lista delle mosse disponibili
		#libero i selezionati
		self.lista_selezionati = []
		#mi popolo la lista delle mosse disponibili
		self.lista_mosse_disponibili = GestisciMosse.calcola_lista_mosse_totale(schema=self.schema_attuale, numero_giocatore=self.giocatore_attuale)
		#me lo stampo
		print(self.lista_mosse_disponibili)
		#verifico se ha vinto un giocatore
		if len(self.lista_mosse_disponibili) == 0:
			#verifico chi ha vinto
			if self.giocatore_attuale == Costanti.numero_giocatore_blu:
				vincente = 'rosso'
			else:
				#ha vinto il blu
				vincente = 'blu'
			messagebox = QtWidgets.QMessageBox()
			messagebox.setIcon(QtWidgets.QMessageBox.Information)
			messagebox.setText('VITTORIA')
			messagebox.setInformativeText(f'Ha vinto il giocatore {vincente}')
			messagebox.setStandardButtons(QtWidgets.QMessageBox.Ok)
			messagebox.setDefaultButton(QtWidgets.QMessageBox.Ok)
			messagebox.exec()


					


	##############################################################################################################################################################
	#metodi generali

	def casella_tabella_cliccata(self, riga, colonna):
		#metodo per gestire la pressione di una casella con la modalita tabella
		#ho trovato riga e colonna
		tupla_trovata = (riga, colonna)
		print(tupla_trovata)
		#verifico se sono autorizzato a premere
		if ( (self.giocatore_attuale == Costanti.numero_giocatore_blu) and (self.tipologia_giocatore_blu == Costanti.numero_tipologia_giocatore_umano) ) or ( (self.giocatore_attuale == Costanti.numero_giocatore_rosso) and (self.tipologia_giocatore_rosso == Costanti.numero_tipologia_giocatore_umano)):
			#verifico se ho scelto di deselezionare una case3lla
			if (len(self.lista_selezionati) >0 ) and (tupla_trovata == self.lista_selezionati[-1]):
				#posso rimuoverla
				self.lista_selezionati.remove(tupla_trovata)
				#a questo punto riaggiorno tutto
				self.aggiorna_schema_grafica()
			else:
				#verifico se ho selezionato un path accettabile
				numero_elementi = len(self.lista_selezionati) +1
				if (self.lista_selezionati + [tupla_trovata]) in [ mossa[:numero_elementi] for mossa in self.lista_mosse_disponibili ]:
					#significa che ho selezionato un path accettabile
					#se sono a meta di una mossa la eseguo
					if len(self.lista_selezionati)>0:
						ultima_casella = self.lista_selezionati[-1]
						(nuovo_schema, punteggio) = GestisciMosse.esegui_mossa_singola(schema=self.schema_attuale, origine=ultima_casella, destinazione=tupla_trovata)
						self.schema_attuale = nuovo_schema
					#aggiungo la tupla a quelle selezionate
					self.lista_selezionati.append(tupla_trovata)
					#adesso devo verificare se ho completato una mossa
					if self.lista_selezionati in self.lista_mosse_disponibili:
						#mossa completata
						#nuovo_thread = QtCore.QThread(target=self.termina_turno_utente)
						#nuovo_thread.start()
						self.termina_turno_utente()
					else:
						#a questo punto riaggiorno tutto
						self.aggiorna_schema_grafica()


	def termina_turno_utente(self):
		#metodo per terminare il turno dell'utente
		#verifico se ha fatto nuovi damoni()
		(r,c) = self.lista_selezionati[-1]
		(nuovo_schema, punteggio) = GestisciMosse.verifica_nuovi_damoni(schema=self.schema_attuale, r_u=r, c_u=c)
		self.schema_attuale = nuovo_schema
		#inverto il giocatore attuale
		if self.giocatore_attuale == Costanti.numero_giocatore_blu:
			self.giocatore_attuale = Costanti.numero_giocatore_rosso
		else:
			#ha giocato il rosso -> passo al blu
			self.giocatore_attuale = Costanti.numero_giocatore_blu
		#mostro lo schema cosi come e
		self.aggiorna_schema_grafica()
		#ribalto sempre lo schema
		schema_ribaltato = GestisciMosse.ruota_schema(schema=self.schema_attuale)
		self.schema_attuale = schema_ribaltato
		#inizializzo il turno
		self.inizializza_turno()
		if (self.tipologia_giocatore_blu == Costanti.numero_tipologia_giocatore_umano) and (self.tipologia_giocatore_rosso == Costanti.numero_tipologia_giocatore_umano):
			#se umano contro umano non faccio niente
			self.aggiorna_schema_grafica()
		else:
			#ho scelto di giocare contro pc
			#utilizzo la difficoltà segnata dall'utente di volta in volta
			self.aggiorna_difficolta()
			#self.esegui_mossa_computer()
			#eseguo thread solo quando faccio mossa del computer
			# Step 2: Create a QThread object
			self.thread = QtCore.QThread()
			# Step 3: Create a worker object
			self.worker = Worker()
			# Step 4: Move worker to the thread
			self.worker.moveToThread(self.thread)
			# Step 5: Connect signals and slots
			self.thread.started.connect(lambda : self.worker.esegui_mossa_computer(oggetto_partita=self))
			self.worker.finished.connect(self.thread.quit)
			self.worker.finished.connect(self.worker.deleteLater)
			self.thread.finished.connect(self.thread.deleteLater)
			self.worker.aggiornamento.connect(self.aggiorna_schema_grafica)
			# Step 6: Start the thread
			self.thread.start()

	#############################################################################################################################################################################
	#metodi per gestire la difficolta
	def abilita_disabilita_difficolta(self):
		#metodo per abilitare o meno lo spinbox della difficolta
		valore_cambio_difficolta = self.grafica.spinBox_pedine_cambio_difficolta.value()
		if (type(valore_cambio_difficolta) == int) and (valore_cambio_difficolta >0):
			self.grafica.spinBox_profondita.setDisabled(True)
		else:
			self.grafica.spinBox_profondita.setEnabled(True)

	def aggiorna_difficolta(self):
		#metodo per aggiornare la profondita tenendo conto dell'eventuale incremento
		#se sto giocnado umano contro umano non ha senso aggiornare la difficolta
		if (self.tipologia_giocatore_blu == Costanti.numero_tipologia_giocatore_umano) and (self.tipologia_giocatore_rosso == Costanti.numero_tipologia_giocatore_umano):
			return
		elif self.tipologia_giocatore_blu == Costanti.numero_tipologia_giocatore_computer:
			#il computer e il blu
			numero_pc = Costanti.numero_giocatore_blu
		else:
			#il computer e il rosso
			numero_pc = Costanti.numero_giocatore_rosso
		#altrimenti ha senso aggiornare difficolta
		valore_cambio_difficolta = self.grafica.spinBox_pedine_cambio_difficolta.value()
		if (type(valore_cambio_difficolta) == int) and (valore_cambio_difficolta >0):
			#allora devo calcolare la nuova difficolta
			#mi calcolo quante sono le pedine attuali del pc
			pedine_attuali = 0
			for r in range(len(self.schema_attuale)):
				for c in range(len(self.schema_attuale[0])):
					casella = self.schema_attuale[r][c]
					if (abs(casella) in [Costanti.numero_identificativo_pedina, Costanti.numero_identificativo_damone]) and (casella *numero_pc >0):
						pedine_attuali += 1
			#calcolo quante pedine sono state mangiate
			pedine_mangiate = self.pedine_iniziali - pedine_attuali
			#calcolo di quanto devo incrementare il livello
			incremento_livello = pedine_mangiate // valore_cambio_difficolta
			#adesso vado ad aggiornare lo spinbox
			self.grafica.spinBox_profondita.setValue(self.profondita_iniziale + incremento_livello)
		#adesso aggiorno la difficolta
		try:
			self.profondita = int(self.grafica.spinBox_profondita.value())
		except:
			pass


	


		